# Sacch_vitamins

This Repository holds the scripts required to generate some figures on the review manuscript entitled "Vitamin growth factor requirements and biosynthesis in *Saccharomyces cerevisiae*"

## Getting started



### Prerequisites


### Installing


## Author

Raul A. Ortiz-Merino

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details


