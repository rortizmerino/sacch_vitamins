#!/usr/bin/perl

use strict;
use warnings;

my ($OS, $OX, $count);
my ($out1, ,$out2, $infile);
open $out1,'>', "databases/mock_uniprot/4930_plus_Saccharomyces_WGS.fasta" or die "$!";
open $out2,'>', "databases/mock_uniprot/4930_plus_Saccharomyces_WGS.taxids.txt" or die "$!";

$OS="Saccharomyces paradoxus CBS432"; $OX=226125; $count=0;
open $infile, '<', "otherDBs/CBS432.pep.fa" or die "$!";
while (<$infile>) {
 if ($_ =~ />/){
  chomp;
  $_ =~ s/>//;
  @_ = split(/\|/,$_);
  $_[0] =~ s/CBS432_//;
  $_[1] =~ s/CBS432_//;
  print $out1 ">Spar|$_[0]|$_[1] $_[2] OS=$OS OX=$OX $_\n";
  $count++;
 }
 else {print $out1 $_;}
}
print $out2 "$OX\t$count\t$OS\n";

$OS="Saccharomyces arboricola CBS10644"; $OX="1160507_2"; $count=0;
open $infile, '<', "otherDBs/S_arb_all_proteins.fasta" or die "$!";
while (<$infile>) {
 if ($_ =~ />/){
  $_ =~ s/>//; $_ =~ s/;Note/; Note/;
  @_ = split(/\s/,$_);
  my $ID = $_[0];
  my ($Alias) = $_ =~ m/Alias=.+;/g ; $Alias =~ s/(Alias=)|(;)//g;
  @_ = split(/\s/,$Alias);
  my $tmp = $_[0]; $Alias = $tmp;
  if ($Alias eq "") {$Alias = $ID;}
  print $out1 ">Sarb|$ID|$Alias $Alias OS=$OS OX=$OX $_";
  $count++;
 }
 else {print $out1 $_;}
}
print $out2 "$OX\t$count\t$OS\n";

$OS="Saccharomyces uvarum CBS7001"; $OX=226127; $count=0;
open $infile, '<', "otherDBs/Sbay.aa" or die "$!";
while (<$infile>) {
 if ($_ =~ />/){
  chomp;
  $_ =~ s/>//;
  (my $ID = $_) =~ s/Sbay_//g ;
  print $out1 ">Suva|$ID|$ID OS=$OS OX=$OX $_\n";
  $count++;
 }
 else {print $out1 $_;}
}
print $out2 "$OX\t$count\t$OS\n";

$OS="Saccharomyces mikatae CBS14759"; $OX=226126; $count=0;
open $infile, '<', "otherDBs/Smik.aa" or die "$!";
while (<$infile>) {
 if ($_ =~ />/){
  chomp;
  $_ =~ s/>//;
  (my $ID = $_) =~ s/Smik_//g ;
  print $out1 ">Smik|$ID|$ID OS=$OS OX=$OX $_\n";
  $count++;
 }
 else {print $out1 $_;}
}
print $out2 "$OX\t$count\t$OS\n";

$OS="Saccharomyces jurei CBS14759"; $OX=1987369; $count=0;
open $infile, '<', "otherDBs/Sjur.aa.fa" or die "$!";
while (<$infile>) {
 if ($_ =~ />/){
  chomp;
  $_ =~ s/>//;
  @_ = split(/\s/,$_);
  (my $ID = $_[0]) =~ s/SACJUREI_LOCUS//g ;
  print $out1 ">Sjur|$ID|$ID OS=$OS OX=$OX $_\n";
  $count++;
 }
 else {print $out1 $_;}
}
print $out2 "$OX\t$count\t$OS\n";

$OS="Saccharomyces cerevisiae CEN.PK113-7D_2"; $OX="889517_2"; $count=0;
open $infile, '<', "otherDBs/CENPK113-7D_20170627.annotated.proteins.fasta" or die "$!";
while (<$infile>) {
 if ($_ =~ />/){
  $_ =~ s/>//;
  @_ = split(/-/,$_);
  my $ID = $_[5];
  my $chr = "";
  if ($_[3] =~ /micron/)      { $chr = "2m"; $ID = $_[6]; }
  elsif ($_[2] =~ /mtDNA/)    { $chr = "mt"; }
  elsif ($_[2] =~ /telomere_/){ ($chr) = $_[2] =~ m/telomere_\d+_/g ; $chr =~ s/(telomere_)|(_)//g; $chr .= "ut" }
  elsif ($_[2] =~ /7D_chr/)   { ($chr) = $_[2] =~ m/7D_chr\d+_/g ; $chr =~ s/(7D_chr)|(_)//g; }
  @_ = split(/\s/,$_);
  my $Alias = $_[4];
  print $out1 ">Scen|$chr.$ID|$Alias $Alias OS=$OS OX=$OX $_";
  $count++;
 }
 else {print $out1 $_;}
}
close $out1;
print $out2 "$OX\t$count\t$OS\n";

my $cmd;
$cmd = "./scripts/fmt_DBs.sh"; system ($cmd);

# insert K7_BIO1-3 and K7_BIO6-3 as Scer
#Scer
$OS="Saccharomyces cerevisiae S288C"; $OX="559292"; $count=0;
#=pod
open $out1,'>>', "databases/mock_uniprot/4930_plus_Saccharomyces_WGS.fasta" or die "$!";
open $infile, '<', "queries/G2WFX1.fasta" or die "$!";
while (<$infile>) {
 if ($_ =~ />/){ @_ = split(/\s/,$_); print $out1 "$_[0] $_[1] OS=$OS OX=$OX\n"; $count++; }
 else { print $out1 "$_"; }
}

open $infile, '<', "queries/G2WFX2.fasta" or die "$!";
while (<$infile>) {
 if ($_ =~ />/){ @_ = split(/\s/,$_); print $out1 "$_[0] $_[1] OS=$OS OX=$OX\n"; $count++; }
 else { print $out1 "$_"; }
}
close $out1;
#=cut
open $infile, '<', "databases/mock_uniprot/4930_all_Saccharomyces_WGS.fasta" or die "$!";
while (<$infile>) { if ($_ =~ />/ && $_ =~ /OX=$OX /){ $count++; } }
print $out2 "$OX\t$count\t$OS\n";

#Seub
$OS="Saccharomyces eubayanus"; $OX="1080349"; $count=0;
open $infile, '<', "databases/mock_uniprot/4930_all_Saccharomyces_WGS.fasta" or die "$!";
while (<$infile>) { if ($_ =~ />/ && $_ =~ /OX=$OX /){ $count++; } }
print $out2 "$OX\t$count\t$OS\n";

#Skud
$OS="Saccharomyces kudriavzevii IFO 1802"; $OX="226230"; $count=0;
open $infile, '<', "databases/mock_uniprot/4930_all_Saccharomyces_WGS.fasta" or die "$!";
while (<$infile>) { if ($_ =~ />/ && $_ =~ /OX=$OX /){ $count++; } }
print $out2 "$OX\t$count\t$OS\n";

#Sarb
$OS="Saccharomyces arboricola H-6"; $OX="1160507"; $count=0;
open $infile, '<', "databases/mock_uniprot/4930_all_Saccharomyces_WGS.fasta" or die "$!";
while (<$infile>) { if ($_ =~ />/ && $_ =~ /OX=$OX /){ $count++; } }
print $out2 "$OX\t$count\t$OS\n";

#Scen
$OS="Saccharomyces cerevisiae CEN.PK113-7D"; $OX="889517"; $count=0;
open $infile, '<', "databases/mock_uniprot/4930_all_Saccharomyces_WGS.fasta" or die "$!";
while (<$infile>) { if ($_ =~ />/ && $_ =~ /OX=$OX /){ $count++; } }
print $out2 "$OX\t$count\t$OS\n";

