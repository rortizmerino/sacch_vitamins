#!/usr/bin/perl

use strict;
use warnings;

my $usage = "\nUsage: get_n_fltrHMMERhits.pl <download> <run> <eval> <len>\n";
$usage .= " download 0 do not download\n";
$usage .= " run 0 do not run\n";
$usage .= " eval 10 default, 1e-5 recommended\n";
$usage .= " alignment length cutoff 0 default, 75 recommended\n\n";

die $usage unless @ARGV > 0;

my ($dwnld, $run, $eval, $lenc, $cmd);
if (defined $ARGV[0]) { $dwnld= $ARGV[0] } else {$dwnld=1};
if (defined $ARGV[1]) { $run  = $ARGV[1] } else {$run = 1};
if (defined $ARGV[2]) { $eval = $ARGV[2] } else {$eval = 1e-5};
if (defined $ARGV[3]) { $lenc = $ARGV[3] } else {$lenc = 0};

my (@ids, $infile);
open $infile, '<', "vitamin_genes_UNIPROT_IDs.txt" or die "$!";
while (<$infile>){
 chomp;
 if ($_ !~ /Gene/){ @_ = split (/\t/,$_); push (@ids, $_[2]); }
}

my %txdNsp;
open $infile, '<', "databases/mock_uniprot/4930_plus_Saccharomyces_WGS.taxids.txt" or die "$!";
while (<$infile>){
 chomp;
 @_ = split (/\t/,$_); $txdNsp{$_[0]} = $_[2];
}

my $c = 1;
foreach my $id (@ids) {
my (%hitsNlen, %hitsNeval, %hitsNtxd, %hitsNalPct);
if ($c >= 1){

 $cmd = "wget https://www.uniprot.org/uniprot/$id.fasta --output-document=queries/$id.fasta";
 if ($dwnld == 1) {system ($cmd);}

 $cmd = "cp queries/$id.fasta . "; if ($run == 1) {system ($cmd);}
 $cmd = "perl scripts/localHMMERsearch_allPlus.pl $id.fasta phmmer 4930_plus $eval";
                                   if ($run == 1) {system ($cmd);}
 $cmd = "rm $id.fasta";            if ($run == 1) {system ($cmd);}
 $cmd = "mv $id.* results/.";      if ($run == 1) {system ($cmd);}

#if ($c == 1){
 my ($qlen, $al_pct);
 open $infile, '<', "results/$id.phmmer.4930_plus.dom.tab" or die "$!";
 while (<$infile>){
  unless ($_ =~ /^#/) {
   @_ = split (/\s+/, $_);
   $qlen = $_[5];
   my ($acc) = $_[0] =~ m/\|.+\|/g ; $acc =~ s/\|//g;
   my $alstrt = $_[17];
   my $alstop = $_[18];
   my $al_len = $alstop - $alstrt;
#   reactivate when database duplicates are removed
=pod
#   if ($hitsNlen{$acc}) {$hitsNlen{$acc} .= ",$al_len";}
#   else {$hitsNlen{$acc} = $al_len;}
=cut
   my ($txd) = $_ =~ m/OX=\w+/g ; $txd =~ s/OX=//g;
   $hitsNtxd{$acc} = $txd;  
   $hitsNlen{$acc} = $al_len + 1;
   $hitsNeval{$acc} = $_[6];
  }
 }

 my (@data);
 while (my ($k,$v)=each %hitsNlen){
#   reactivate when database duplicates are removed
=pod
  if ($v =~ /,/) {
   my @vals = split (/,/,$v);
   my $agg = 0; $agg += $_ for @vals;
   print " $agg\n";
  }
  else {print " $qlen\n";}
=cut
  my $n = $v * 100 / $qlen;
  if ($n =~ /\./) { $al_pct = substr($n,0,index($n,'.') + 1 + 2); }
  else { $al_pct = "$n.00" }
  $hitsNalPct{$k} = $al_pct;
  if ($al_pct > $lenc) { push (@data, [$al_pct, $hitsNeval{$k}, $k, $hitsNtxd{$k}]); }
 }
                     #increasing eval        decreasing al_pct
 my @sorted = sort { $a->[1] <=> $b->[1] || $b->[0] <=> $a->[0]} @data ;
                     #decreasing al_pct      increasing eval
# my @sorted = sort { $b->[0] <=> $a->[0] || $a->[1] <=> $b->[1] } @data ;
 my $arrlen = scalar @sorted;

 open my $outfile, '>', "results/$id.phmmer.4930_plus.sorted.hits" or die "$!";
 while (my ($k,$v)=each %txdNsp){
  for (my $i = 0; $i < $arrlen; $i++){
   if ( $k eq @{ $sorted[$i] }[3]){
    print $outfile "@{ $sorted[$i] }[0]\t@{ $sorted[$i] }[1]\t@{ $sorted[$i] }[2]\t$k\t$v\n";
   }
  }
 }

}

$c++;
}
