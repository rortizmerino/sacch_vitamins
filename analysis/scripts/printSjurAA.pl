#!/usr/bin/perl

use strict;
use warnings;

use Bio::Perl;
use Bio::DB::Fasta;
use Bio::Seq;
use Bio::SeqIO;

my $usage = "\nUsage: printSjurAA.pl\n";
$usage .= " \n";

#die $usage unless @ARGV > 0;

open my $infile, '<', "GCA_900290405.1_SacJureiUoM1_genomic.gbff" or die "$!";
#open my $outfile, '>', "" or die "$!";

my ($chr, $gene, $coords);
my (%gene_n_coords, %gene_n_chr, %repeats);

while (<$infile>){
 if ($_ =~ /^VERSION/){ @_ = split(/\s/,$_); $chr = $_[$#_]; }
 if ($_ =~ /^\s+gene/){ @_ = split(/\s/,$_); $coords = $_[$#_]; }
 if ($_ =~ /^\s+\/locus_tag/){
  @_ = split(/"/,$_);
  $gene = $_[1];
  $gene_n_coords{$gene} = $coords;
  $gene_n_chr{$gene} = $chr;
 }
 if ($_ =~ /^\s+repeat_region/){ $repeats{$gene} = ""; }
}

my $seq_in = "GCA_900290405.1_SacJureiUoM1_genomic.fna";
my $inseq_db = Bio::DB::Fasta->new($seq_in);
my $out_seq = Bio::SeqIO->new(-file => ">Sjur.aa.fa", -format => 'fasta', );

while (my ($g,$c)=each %gene_n_coords){
 unless (exists $repeats{$g}){
  my $seq = $inseq_db -> get_Seq_by_id($gene_n_chr{$g});
  @_ = split(/\.\./,$c);
  if ($c =~ /complement/){ $_[0] =~ s/complement\(//; $_[$#_] =~ s/\)//; }
  my $subseq = $seq -> subseq($_[0], $_[$#_]);
  my $display_id = "$g $gene_n_chr{$g} $c";
  my $dna_seq_obj = Bio::Seq->new(-seq => $subseq, -display_id => $display_id);
  if ($c =~ /complement/){ my $tmp = $dna_seq_obj -> revcom; $dna_seq_obj = $tmp; }
  my $out_seq_obj = $dna_seq_obj -> translate;
  $out_seq -> verbose(-1);
  $out_seq_obj -> verbose(-1);
  $out_seq -> write_seq($out_seq_obj);
 }
}
