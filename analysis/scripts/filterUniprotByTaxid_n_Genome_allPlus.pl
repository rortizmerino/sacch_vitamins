#!/usr/bin/perl

use strict;
use warnings;

use Bio::Perl;
use Bio::DB::Fasta;
use Bio::Seq;
use Bio::SeqIO;

my $strng;
$strng .= "2:      Bacteria\n";
$strng .= "2157:   Archaea\n";
$strng .= "2759:   Eukaryota\n";
$strng .= "4751:   Fungi\n";
$strng .= "4890:   Ascomycota\n";
$strng .= "147537: Saccharomycotina\n";
$strng .= "4930:   Saccharomyces\n";
$strng .= "451455: Neocallimastigomycetes\n";
die "usage: filterUniprotByTaxid_n_Genome_allPlus.pl <tax_id> <refprot|sprot|trembl>\n$strng" unless @ARGV > 0;

my (%WGS_IDs, %taxid_name, %taxid_lineage, %tax_lng, %kept_txds);
my ($db_path, $infile, $in, $tax_id, $lineage, $out);

my $trgt_txd = $ARGV[0];
my $dbtype = $ARGV[1];
$db_path = "/data/raul/hmmer/Sacc_vitamins/databases";

my $datestring;
$datestring = localtime();
print "filterUniprotByTaxid_n_Genome_allPlus.pl $trgt_txd $dbtype started at $datestring\n";

$infile = "$db_path/mock_ncbi/assembly_summary_genbank.txt";
open $in, '<', $infile or die "$!";
while (<$in>){
 unless ($_ =~ /^#/){
  @_ = split (/\t/, $_);
  # $_[4] : reference, repressentative or no category status
  # $_[11]: complete, chromosome, scaffold, contig
  # $_[13]: full, partial
  # only filter out genomes with no status if db is not trembl
  if ($dbtype eq "trembl" || $dbtype eq "all"){
   if ($_[11] ne "Contig" && $_[13] ne "Partial"){ $WGS_IDs{$_[5]}=$_[6]; }
  }
  else { if ($_[4] ne "na" && $_[11] ne "Contig" && $_[13] ne "Partial"){ $WGS_IDs{$_[5]}=$_[6]; } }
 }
}
close $in;
print "Passed: $WGS_IDs{$trgt_txd}\n";
$infile = "$db_path/mock_ncbi/mock_taxdump/fullnamelineage.dmp";
open $in, '<', $infile or die "$!";
while (<$in>){
 @_ = split (/\|/, $_);
 $_[0] =~ s/\s+$//g;
 $_[1] =~ s/^\s+|\s+$//g;
 $_[2] =~ s/^\s+|\s+$//g;
 $taxid_name{$_[0]} = $_[1];
 $taxid_lineage{$_[0]} = $_[2];
}
close $in;

$infile = "$db_path/mock_ncbi/mock_taxdump/taxidlineage.dmp";
open $in, '<', $infile or die "$!";
while (<$in>){
 @_ = split (/\|/, $_);
 ($tax_id = $_[0]) =~ s/\s+$//g;
 ($lineage = $_[1]) =~ s/^\s+|\s+$//g;
 $tax_lng{$tax_id} = " $lineage ";
 if ($tax_lng{$tax_id} =~ / $trgt_txd /){
  if ($tax_id eq $trgt_txd || exists $WGS_IDs{$tax_id}) {$kept_txds{$tax_id} = $taxid_name{$tax_id};}
 }
}
close $in;

$taxid_name{$trgt_txd} =~ s/\s/_/g;
print "Target taxid: $trgt_txd\t$taxid_name{$trgt_txd}\n";
print "Kept: $kept_txds{$tax_id}\n";
my ($seq_in, $outfile, $seq_out);
$seq_in = $outfile = $seq_out = "$db_path/mock_uniprot/";
if ($dbtype eq "sprot"){ 
 $outfile .= $trgt_txd."_s_"."$taxid_name{$trgt_txd}"."_WGS.taxids.txt";
 $seq_out .= "$trgt_txd"."_s_"."$taxid_name{$trgt_txd}"."_WGS.fasta";
 $seq_in .= "uniprot_sprot.fasta";
}
if ($dbtype eq "refprot"){
 $outfile .= $trgt_txd."_ref_"."$taxid_name{$trgt_txd}"."_WGS.taxids.txt";
 $seq_out .= "$trgt_txd"."_ref_"."$taxid_name{$trgt_txd}"."_WGS.fasta";
 $seq_in .= "uniprotrefprot.fasta";
}
if ($dbtype eq "trembl"){
 $outfile .= $trgt_txd."_t_"."$taxid_name{$trgt_txd}"."_WGS.taxids.txt";
 $seq_out .= "$trgt_txd"."_t_"."$taxid_name{$trgt_txd}"."_WGS.fasta";
 $seq_in .= "uniprot_trembl.fasta";
}
if ($dbtype eq "all"){
 $outfile .= $trgt_txd."_all_"."$taxid_name{$trgt_txd}"."_WGS.taxids.txt";
 $seq_out .= "$trgt_txd"."_all_"."$taxid_name{$trgt_txd}"."_WGS.fasta";
# $seq_in .= "uniprot_all.fasta";
 $seq_in .= "4930_all_Saccharomyces_WGS.fasta";
}
print "Using $seq_in\n";
my %WGS_n_prot;
my $out_seq = Bio::SeqIO->new(-file => ">$seq_out", -format => 'fasta', );
my $inseq = Bio::SeqIO->new(-file => "$seq_in", -format => 'fasta', );

while (my $seq = $inseq -> next_seq){
 my ($ox) = $seq -> desc =~ m/OX=\d+/g ; $ox =~ s/OX=//;
 if (exists $kept_txds{$ox}){
  my $out_seq_obj = Bio::Seq->new(-seq => $seq -> seq(), -display_id => $seq -> id ." ". $seq -> desc);
  $out_seq -> verbose(-1);
  $out_seq -> write_seq($out_seq_obj);
  if (exists $WGS_n_prot{$ox}) {$WGS_n_prot{$ox} = $WGS_n_prot{$ox} + 1;}
  else {$WGS_n_prot{$ox} = 1;}
 }
 else {$WGS_n_prot{$ox} = 0;}
}

# moved from above to account for genomes without proteome
open $out, '>', $outfile or die "$!";
while (my ($k,$v)=each %kept_txds){
 if (exists $WGS_n_prot{$k} && $WGS_n_prot{$k} > 0){print $out "$k\t$WGS_n_prot{$k}\t$v\n";}
 else {print $out "$k\t0\t$v\n";}
}
close $out;

$datestring = localtime();
print "filterUniprotByTaxid_n_Genome.pl $trgt_txd $dbtype finished at $datestring\n";

exit;
