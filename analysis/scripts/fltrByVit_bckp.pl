#!/usr/bin/perl

use strict;
use warnings;

my $usage = "\nUsage: fltrByVit.pl\n";

my ($infile, @ids, @vits, %vits, %idsNvits);
open $infile, '<', "vitamin_genes_UNIPROT_IDs.txt" or die "$!";
while (<$infile>){
 chomp;
 if ($_ !~ /Gene/){ 
  @_ = split (/\t/,$_); push (@ids, $_[2]); $idsNvits{$_[2]} = $_[1];
  unless (exists $vits{$_[1]}) {$vits{$_[1]} = 0;}
  if ($vits{$_[1]} == 0) { push (@vits, $_[1]); my $tmp = $vits{$_[1]}; $vits{$_[1]} = $tmp + 1 ;}
 }
}

my %txdNsp;
open $infile, '<', "databases/mock_uniprot/4930_plus_Saccharomyces_WGS.taxids.txt" or die "$!";
while (<$infile>){ chomp; @_ = split (/\t/,$_); $txdNsp{$_[0]} = $_[2]; }

foreach my $k (sort keys %txdNsp){
 open my $out1, '>', "bestHitIDs.$k.txt" or die "$!";
 open my $out2, '>', "bestHitCounts.$k.tab" or die "$!";
 print $out2 "ID\t$txdNsp{$k}\n"; 

 foreach my $vit (@vits) {
  my @hitsPerVit = ();
  my %bestHitPerQuery;
  print $out1 "$vit\n";
  foreach my $id (@ids) {
   if ($idsNvits{$id} eq $vit){
    print $out1 " $id\n";
    open my $infile, '<', "results/$id.phmmer.4930_plus.sorted.hits" or die "$!";
    while (<$infile>) {
     @_ = split (/\t/, $_);
     if ($_[3] eq $k) {
      print $out1 "  $_";      
                         #alpct   eval  hitid  query
      push (@hitsPerVit, [$_[0], $_[1], $_[2], $id]);
     }
    }
   }
  }
  print $out1 "\n";

  my (%usedHits, %hitsNvals, %multiHits);
                     #increasing eval        decreasing al_pct
  my @sorted = sort { $a->[1] <=> $b->[1] || $b->[0] <=> $a->[0]} @hitsPerVit ;
                     #decreasing al_pct      increasing eval
#  my @sorted = sort { $b->[0] <=> $a->[0] || $a->[1] <=> $b->[1] } @hitsPerVit ;
  my $arrlen = scalar @sorted ;
  for (my $i = 0; $i < $arrlen; $i++){
   print $out1 " => @{ $sorted[$i] }[0]\t@{ $sorted[$i] }[1]\t@{ $sorted[$i] }[2]\t@{ $sorted[$i] }[3]\n";
   my $query = @{ $sorted[$i] }[3];
   my $hit   = @{ $sorted[$i] }[2];
   $hitsNvals{$hit} = "@{ $sorted[$i] }[0]\t@{ $sorted[$i] }[1]";
   if ($bestHitPerQuery{$query}) {
    if ($hitsNvals{$bestHitPerQuery{$query}} eq $hitsNvals{$hit}) {
     if ($multiHits{$query}) { $multiHits{$query} .= ", $hit"; }
     else { $multiHits{$query} = "$bestHitPerQuery{$query}, $hit"; }
    }
   }
   else {$bestHitPerQuery{$query} = $hit;}
  }
  print $out1 "\n";

  foreach my $id2 (@ids) {
   my $count = 0;
   if ($idsNvits{$id2} eq $vit) {
    if ($bestHitPerQuery{$id2}) {
     if ($multiHits{$id2}) {
      print $out1 " query: $id2 bestHits: $multiHits{$id2}\n";
      @_ = split (/, /, $multiHits{$id2});
      foreach my $multiHit (@_) {
       #$usedHits{$multiHit} = $multiHit; $count++;
       unless ($usedHits{$multiHit}) { $count++; }
       $usedHits{$multiHit} = $multiHit;
      }
     }
     else {
      print $out1 " query: $id2 bestHit:  $bestHitPerQuery{$id2}\n";
      unless ($usedHits{$bestHitPerQuery{$id2}}) { $count++; }
      $usedHits{$bestHitPerQuery{$id2}} = $bestHitPerQuery{$id2};
     }
    }
    else { print $out1 " query: $id2 bestHit: None\n"; }
    print $out2 "$id2\t$count\n";
   }
  }
  print $out1 "\n\n";
 }
}
