#!/usr/bin/perl

use strict;
use warnings;

use Bio::Perl;
use Bio::DB::Fasta;
use Bio::Seq;
use Bio::SeqIO;

my $usage = "\nUsage: localHMMERsearch_allPlus.pl <uniprotid|file.fasta> <program:phmmer|jackhmmer> <taxid_db> <eval>\n";
$usage .= " file.fasta: aminoacid sequence to use as query in fasta format\n";
$usage .= " text before .fasta wil be used as ID, please avoid spaces\n";
$usage .= " place the file in the same directory as this program\n";
$usage .= " e.g. P28272\n\n";
$usage .= " default e-value cutoff = 10\n\n";

die $usage unless @ARGV > 0;

#my $db_path = $ENV{"HOME"}."/seqdata/databases/uniprotrefprot/";
#my $ncbi_path = $ENV{"HOME"}."/seqdata/databases/ncbi/new_taxdump/";

my $db_path = "/data/raul/hmmer/Sacc_vitamins/databases/mock_uniprot/";
my $ncbi_path = "/data/raul/hmmer/Sacc_vitamins/databases/mock_ncbi/mock_taxdump/";

my (%taxid_dbs, $taxid_db);
$taxid_db = "none";
opendir(my $dh, $db_path) || die "Can't open $db_path: $!";
while (readdir $dh) {
 if ( $_ =~ /_WGS.fasta$/ ) {
  my @tmp = split (/\//, $_);
  @_ = split(/_/, $tmp[$#tmp]);
  $taxid_dbs{"$_[0]_$_[1]"} = $tmp[$#tmp];
 }
}
$taxid_dbs{"sprot"}   = "uniprot_sprot.fasta";
$taxid_dbs{"refprot"} = "uniprotrefprot.fasta";
$taxid_dbs{"trembl"}  = "uniprot_trembl.fasta";

my $fastaflag = 0;
my $ID = $ARGV[0];
if ($ID =~ /\.fasta$/) { $ID =~ s/\.fasta//g; $fastaflag = 1; print "Executing search from fasta file\n"; }

my $program = "";
if (exists $ARGV[1]) {$program = $ARGV[1]};
if ($program eq "phmmer" || $program eq "jackhmmer") { print "Selected $program search\n"; }
elsif ($program ne "phmmer" || $program ne "jackhmmer") { $program = "phmmer"; print "Default phmmer search\n"};

if (defined $ARGV[2]) {$taxid_db = $ARGV[2]; print "Selected $taxid_dbs{$taxid_db} database\n"};
unless (exists $taxid_dbs{$taxid_db}) {
 print "$usage\n Available <taxid_db> databases:\n";
 while (my ($k,$v) = each %taxid_dbs){ print "\t$k: $v\n"; }
 print "\n";
 exit;
}

my $eval = 10;
if (defined $ARGV[3]) { $eval = $ARGV[3]; }

my $cmd;
my $found_ID = 0;

# get protein info from uniprot
sub getSeq {
 my $seq_in = $db_path.$_[0].".fasta";
 print "Searching $ID in $_[0]\n";
 my $inseq_db = Bio::DB::Fasta->new($seq_in);
 my @ids = $inseq_db -> get_all_primary_ids;
 my %ids;
 foreach (@ids) { 
  if ($_ =~ /\|\w+\|/) { 
   my ($id) = $_ =~ m/\|\w+\|/g ; $id =~ s/\|//g; $ids{$id} = $_; 
   last if ($id eq $ID) ;
  }
 }
 if (exists $ids{$ID}){
  my $seq_out = "$ID.fasta";
  my $out_seq = Bio::SeqIO->new(-file => ">$seq_out", -format => 'fasta', );
  my $seq = $inseq_db -> get_Seq_by_id($ids{$ID});
  my $out_seq_obj = Bio::Seq->new(-seq => $seq -> seq(), -display_id => $seq -> id ." ". $seq -> desc);
  $out_seq -> verbose(-1);
  $out_seq -> write_seq($out_seq_obj);
  $found_ID = 1;
 }
 return $found_ID;
}

# missing trembl search, might not even put it because might take too long
if ($fastaflag == 0) { 
 $found_ID = getSeq("uniprot_sprot");
 if ( $found_ID == 0 ) { 
  print "No entry info found in the swissprot database. Will check in refprot, might take a while\n";
  $found_ID = getSeq("uniprotrefprot");
  if ( $found_ID == 0 ) { 
   print "No entry info found, check your <uniprotid> or execute again with fasta file\n"; 
   exit;
  }
 }
 if ( $found_ID == 1 ) { print "$ID info found and retrieved\n"; }
}

# run HMMER search
$cmd = "$program ";
$cmd .= "--tblout ".$ID.".$program.$taxid_db.tab ";
$cmd .= "--domtblout ".$ID.".$program.$taxid_db.dom.tab ";
if ($program ne "jackhmmer"){
 $cmd .= "--pfamtblout ".$ID.".$program.$taxid_db.pfam.tab ";
}
$cmd .= "-o ".$ID.".$program.$taxid_db.out ";
$cmd .= "-A ".$ID.".$program.$taxid_db.sto ";
$cmd .= "-E $eval ";
$cmd .= $ID.".fasta ";
$cmd .= $db_path."$taxid_dbs{$taxid_db}";
print "Running $program\n";
system $cmd;

# parse HMMER results 
my (%hits_n_coords, %hits_n_species, %txd_n_hits);

open my $infile, '<', $ID.".$program.$taxid_db.dom.tab" or die "$!";
open my $outfile1, '>', $ID.".$program.$taxid_db.hits" or die "$!";

while (<$infile>){
 unless ($_ =~ /^#/){
  @_ = split (/\s+/, $_);
  my $outstr;
#  my ($acc) = $_ =~ m/\|\w+|\d+\|/g ; $acc =~ s/\|//g;
  my ($acc) = $_[0] =~ m/\|.+\|/g ; $acc =~ s/\|//g;
  my $eval = $_[6];
#  my ($taxid) = $_ =~ m/OX=\d+/g ; $taxid =~ s/OX=//;
  my ($taxid) = $_ =~ m/OX=\w+/g ; $taxid =~ s/OX=//g;
  my ($species) = $_ =~ m/OS=.+ OX/g ; $species =~ s/(OS=)|( OX)//g;
  $outstr = "$acc\t$eval\t$taxid\t";
  for (my $i = 22; $i <= scalar @_ ; $i++) {
   if ($_[$i] =~ /^OS=/) {last;}
   $outstr .= "$_[$i] ";
  }
  $outstr .= "\t$species\n";
  $_[0] =~ s/>//g;
  $hits_n_coords{$_[0]} .= "$_[17] $_[18] ";
  unless (exists $hits_n_species{$_[0]}) { 
   if (exists $txd_n_hits{$taxid}){ $txd_n_hits{$taxid}++;}
   else { $txd_n_hits{$taxid} = 1; }
   print $outfile1 $outstr;
  }
  $hits_n_species{$_[0]} = "$taxid\t$species";
 }
}
close $infile; close $outfile1;

# make taxonomical report
my (%txd_n_name, $txdfilename, %txd_n_fullname);
my $tot_txd_cnt = 0;
($txdfilename = $taxid_dbs{$taxid_db}) =~ s/fasta/taxids.txt/g;
open my $txdfile, '<', $db_path.$txdfilename;
while (<$txdfile>){
 chomp;
 @_ = split(/\t/, $_);
 $txd_n_name{$_[0]} = $_[2]; # from 1 to 2 bc hit count addition
 $tot_txd_cnt++;
}
close $txdfile;

my $namefile = "$ncbi_path"."fullnamelineage.dmp";
open my $in, '<', $namefile or die "$!";
while (<$in>){
 @_ = split (/\|/, $_);
 $_[0] =~ s/\s+$//g;
 $_[1] =~ s/^\s+|\s+$//g;
 $txd_n_fullname{$_[0]} = $_[1];
}
close $namefile;

my %seen_ids;
my $seen_txd_cnt = 0;
open my $report, '>', "$ID.$program.$taxid_db.hits.report";;
open my $lngfile, '<', $ncbi_path."taxidlineage.dmp";
while (<$lngfile>){
 chomp;
 my $space_cnt = 0;
 @_ = split (/\|/, $_);
 (my $tax_id = $_[0]) =~ s/\s+$//g;
 (my $lineage = $_[1]) =~ s/\t//g;
 (my $txd_db_indx = $taxid_db) =~ s/_\w+//g;
 if (exists $txd_n_name{$tax_id}) {
  my @lng_arr = split (/$txd_db_indx /, $lineage); 

  unless (exists $lng_arr[1]) {$lng_arr[1] = $tax_id}; # fix uninitialized value error in some taxidlineage defs
  
  my @lng_arr2 = split (/\s/, $lng_arr[1]);
  foreach my $id (@lng_arr2) {
   if (exists $seen_ids{$id}) { $space_cnt++; }
   else { 
    if ($space_cnt > 0) {
     for (my $i = 1; $i <= $space_cnt; $i++){ $seen_ids{$id} .= " "; }
     $space_cnt++;
    }
    if ($space_cnt == 0) {$seen_ids{$id} = ""; $space_cnt++; }
    print $report "$seen_ids{$id}"."$txd_n_fullname{$id}\n";
   }
  }

  for (my $i = 1; $i <= $space_cnt; $i++){ print $report " "; }
  if (exists $txd_n_hits{$tax_id}){ 
   $seen_txd_cnt++;
   print $report "$tax_id $txd_n_hits{$tax_id} $txd_n_name{$tax_id}\n";
  }
  else {print $report "$tax_id 0 $txd_n_name{$tax_id}\n";}
 }
}
print "Species analysed: $tot_txd_cnt\nSpecies with significant hit: $seen_txd_cnt\n";
print $report "Species analysed: $tot_txd_cnt\nSpecies with significant hit: $seen_txd_cnt\n";

my $datestring = localtime();
#print "Indexing started $datestring\n";
my %ids;
my $seq_in = $db_path."$taxid_dbs{$taxid_db}";
my $inseq_db = Bio::DB::Fasta->new($seq_in);
my @ids = $inseq_db -> get_all_primary_ids;
$ids{$_}++ for (@ids);
#print "Indexing finished $datestring\n";

my $seq_out = "$ID.$program.$taxid_db.fasta";
my $out_seq = Bio::SeqIO->new(-file => ">$seq_out", -format => 'fasta', );

my $c_seq_out = "$ID.$program.$taxid_db.cropped.fasta";
my $c_out_seq = Bio::SeqIO->new(-file => ">$c_seq_out", -format => 'fasta', );

open my $outfile2, '>', $ID.".$program.$taxid_db.coords" or die "$!";
while (my ($k,$v) = each %hits_n_coords){
 print $outfile2 "$k\t$hits_n_species{$k}\t$v\n"; 

 if (exists $ids{$k}){
  my $seq = $inseq_db -> get_Seq_by_id($k);
  my $display_id = $seq -> id ." ".$seq -> desc;
  my $out_seq_obj = Bio::Seq->new(-seq => $seq -> seq(), -display_id => $display_id);
  $out_seq -> verbose(-1);
  $out_seq_obj -> verbose(-1);
  $out_seq -> write_seq($out_seq_obj);

  @_ = split (/\s/, $hits_n_coords{$k});
  my $subseq = $out_seq_obj -> subseq($_[0], $_[$#_]);
  my $display_id2 = $seq -> id ."|($_[0]..$_[$#_]) ".$seq -> desc;
  my $c_out_seq_obj = Bio::Seq->new(-seq => $subseq, -display_id => $display_id2);
  $c_out_seq->verbose(-1);
  $c_out_seq_obj->verbose(-1);
  $c_out_seq->write_seq($c_out_seq_obj);
 }

}
close $outfile2;
